﻿using ZenithWeb.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZenithWeb.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();

            string adminId = "";

            string adminRole = "Admin";
            string adminDesc = "The administrator role";

            string memberRole = "Member";
            string memberDesc = "The member role";

            string password = "P@$$w0rd"; 

            // Create roles
            if(await roleManager.FindByNameAsync(adminRole) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(adminRole, adminDesc, DateTime.Now));
            }

            if(await roleManager.FindByNameAsync(memberRole) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(memberRole, memberDesc, DateTime.Now));
            }

            // Create admin user
            if(await userManager.FindByNameAsync("a") == null)
            {
                var adminUser = new ApplicationUser
                {
                    UserName = "a",
                    Email = "a@a.a",
                    FirstName = "Ad",
                    LastName = "Min",
                    Street = "Admin Street",
                    City = "Admin City",
                    Province = "Admin Province",
                    PostalCode = "A1A 1A1",
                    Country = "Adminada",
                    MobileNumber = "6040000000"
                };

                var result = await userManager.CreateAsync(adminUser);

                if(result.Succeeded)
                {
                    await userManager.AddPasswordAsync(adminUser, password);
                    await userManager.AddToRoleAsync(adminUser, adminRole);
                }
                adminId = adminUser.Id;
            }

            // Create member user
            if (await userManager.FindByNameAsync("m") == null)
            {
                var memberUser = new ApplicationUser
                {
                    UserName = "m",
                    Email = "m@m.m",
                    FirstName = "Mem",
                    LastName = "Ber",
                    Street = "Member Street",
                    City = "Member City",
                    Province = "Member Province",
                    PostalCode = "M1M 1M1",
                    Country = "Memberica",
                    MobileNumber = "6040000001"
                };

                var result = await userManager.CreateAsync(memberUser);

                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(memberUser, password);
                    await userManager.AddToRoleAsync(memberUser, memberRole);
                }
            }
        }
    }
}
