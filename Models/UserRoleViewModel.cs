﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ZenithWeb.Models
{
    [NotMapped]
    public class UserRoleViewModel
    {
        public ApplicationUser User { get; set; }

        public IEnumerable<string> Roles { get; set; }
    }
}
