﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ZenithWeb.Models
{
    [NotMapped]
    public class CompoundInterest
    {
        public double PrincipalInvestment { get; set; }
        public double InterestRate { get; set; }
        public int TimesInterestCompounded { get; set; }
        public int Years { get; set; }

        public double Calculate()
        {
            // 1 + r/n
            double body = 1 + (InterestRate / TimesInterestCompounded);

            // nt
            double exponent = TimesInterestCompounded * Years;

            // P(1 + r/n)^nt
            return PrincipalInvestment * Math.Pow(body, exponent);
        }
    }
}
