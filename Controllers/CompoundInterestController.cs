﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ZenithWeb.Models;

namespace ZenithWeb.Controllers
{
    [Authorize(Roles = "Member, Admin")]
    public class CompoundInterestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(CompoundInterest compoundInterest)
        {
            ViewData["Result"] = "$" + compoundInterest.Calculate().ToString("0.##");

            return View();
        }
    }
}