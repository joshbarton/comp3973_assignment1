﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZenithWeb.Data;
using ZenithWeb.Models;

namespace ZenithWeb.Controllers
{
    public class ApplicationUserController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> userManager;

        public ApplicationUserController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Users"] = _context.Users.ToListAsync();
            ViewData["UserRoles"] = _context.UserRoles.ToListAsync();

            return View(await _context.Users.ToListAsync());
        }
    }
}